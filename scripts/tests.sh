#!/bin/bash

if [[ -d "venv" ]]
then
  echo "Activating venv...";
  source venv/bin/activate;
  echo "venv has been activated!";
fi

echo "Starting tests...";
export PYTHON_ENV="test";
CPU_AMMOUNT=$(nproc)
python -m pytest --cache-clear --cov=keyword_api --cov-report term-missing keyword_api/ -s --numprocesses=$CPU_AMMOUNT;

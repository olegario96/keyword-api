#!/bin/bash

if [[ -d "venv" ]]
then
  echo "Activating venv...";
  source venv/bin/activate;
  echo "venv has been activated!";
fi

echo "Running linter...";

if ! pylint --rcfile=.pylintrc keyword_api
then
  echo "Linter errors detected!";
  exit 1;
else
  echo "No linter issues detected"
fi

exit 0;

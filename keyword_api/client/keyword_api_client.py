import json

import requests

from keyword_api.constants import HTTP_STATUS_CODE_NOT_FOUND

class KeywordApiClient(object):
    '''
    Generic client to make requests to Keyword API endpoints
    '''
    def __init__(self):
        pass

    def post(self, url: str, args: dict) -> dict:
        args['keyword'] = json.dumps(args.get('keyword'))
        response: requests.Response = requests.post(url, data=args)
        if response.status_code == HTTP_STATUS_CODE_NOT_FOUND:
            raise Exception('No API key provided.')

        return response.json()

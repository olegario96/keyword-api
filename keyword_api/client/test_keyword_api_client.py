import pytest
import requests_mock

from keyword_api.constants import HTTP_STATUS_CODE_NOT_FOUND, HTTP_STATUS_CODE_OK, SEARCH_VOLUME_GOOGLE_DEV_URL
from keyword_api.client.keyword_api_client import KeywordApiClient

@pytest.fixture(name='keyword_api_client')
def fixture_keyword_api_client():
    return KeywordApiClient()

def test_post_request_when_page_was_not_found(keyword_api_client):
    with requests_mock.Mocker() as request_mocked:
        request_mocked.post(SEARCH_VOLUME_GOOGLE_DEV_URL, status_code=HTTP_STATUS_CODE_NOT_FOUND)
        with pytest.raises(Exception):
            keyword_api_client.post(SEARCH_VOLUME_GOOGLE_DEV_URL, {})

def test_post_request_when_result_is_ok(keyword_api_client):
    with requests_mock.Mocker() as request_mocked:
        request_mocked.post(SEARCH_VOLUME_GOOGLE_DEV_URL, json={'results': {}}, status_code=HTTP_STATUS_CODE_OK)
        data = keyword_api_client.post(SEARCH_VOLUME_GOOGLE_DEV_URL, {})
        assert isinstance(data, dict)
        assert not data.get('results')

import os

import pytest

from keyword_api.utils.environment import is_production_environment

@pytest.fixture(name='_dev_environment')
def fixture_dev_environment():
    os.environ['PYTHON_ENV'] = 'dev'
    return os

@pytest.fixture(name='_prod_environment')
def fixture_prod_environment():
    os.environ['PYTHON_ENV'] = 'prod'
    return os

def test_is_production_environment_on_dev(_dev_environment):
    assert is_production_environment() is False

def test_is_production_environment_on_prod(_prod_environment):
    assert is_production_environment()

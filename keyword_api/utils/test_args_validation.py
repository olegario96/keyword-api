import pytest

from keyword_api.utils.args_validation import validate_common_args

@pytest.fixture(name='invalid_api_key')
def fixture_invalid_api_key():
    return {'apikey': None}

@pytest.fixture(name='empty_api_key')
def fixture_empty_api_key():
    return {'apikey': ''}

@pytest.fixture(name='empty_keyword_list')
def fixture_empty_keyword_list():
    return {'apikey': 'foo', 'keyword': []}

def test_validate_common_args_with_invalid_api_key(invalid_api_key):
    with pytest.raises(Exception):
        validate_common_args(invalid_api_key)

def test_validate_common_args_with_empty_api_key(empty_api_key):
    with pytest.raises(Exception):
        validate_common_args(empty_api_key)

def test_validate_common_args_with_empty_keyword_list(empty_keyword_list):
    with pytest.raises(Exception):
        validate_common_args(empty_keyword_list)

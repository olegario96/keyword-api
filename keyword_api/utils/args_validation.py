from typing import List

def validate_common_args(args: dict) -> None:
    api_key: str = args.get('apikey')
    if api_key is None or api_key == '':
        raise Exception('Empty API key provided!')

    keyword: List = args.get('keyword')
    if not keyword:
        raise Exception('No keyword provided!')

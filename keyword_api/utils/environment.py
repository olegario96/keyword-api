from os import environ

PRODUCTION = 'prod'

def is_production_environment() -> bool:
    python_env: str = environ.get('PYTHON_ENV')
    return python_env == PRODUCTION

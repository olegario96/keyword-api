# @see https://docs.keywordtool.io/reference#search-volume-amazon-country
COUNTRIES: dict = {
    'UNITED_STATES': 'US',
    'AUSTRALIA': 'AU',
    'BRAZIL': 'BR',
    'CANADA': 'CA',
    'CHINA': 'CN',
    'FRANCE': 'FR',
    'GERMANY': 'DE',
    'INDIA': 'IN',
    'ITALY': 'IT',
    'JAPAN': 'JP',
    'MEXICO': 'MX',
    'NETHERLANDS': 'NL',
    'SPAIN': 'ES',
    'UNITED_KINGDOM': 'GB',
}

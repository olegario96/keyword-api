from copy import deepcopy
from keyword_api.constants.google.google_currency import CURRENCIES as GOOGLE_CURRENCIES

# @see https://docs.keywordtool.io/reference#search-volume-amazon-metrics-currency
CURRENCIES: dict = deepcopy(GOOGLE_CURRENCIES)

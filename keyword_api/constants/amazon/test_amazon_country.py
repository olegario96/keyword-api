from typing import KeysView
from keyword_api.constants import AMAZON_COUNTRIES

def test_countries() -> None:
    keys: KeysView = AMAZON_COUNTRIES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert AMAZON_COUNTRIES[key] is not None

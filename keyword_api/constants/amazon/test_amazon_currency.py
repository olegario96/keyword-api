from typing import KeysView
from keyword_api.constants import AMAZON_CURRENCIES

def test_currencies() -> None:
    keys: KeysView = AMAZON_CURRENCIES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert AMAZON_CURRENCIES[key] is not None

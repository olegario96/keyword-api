from typing import KeysView
from keyword_api.constants import YOUTUBE_COUNTRIES

def test_countries() -> None:
    keys: KeysView = YOUTUBE_COUNTRIES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert YOUTUBE_COUNTRIES[key] is not None

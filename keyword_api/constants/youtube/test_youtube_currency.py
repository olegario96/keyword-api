from typing import KeysView
from keyword_api.constants import YOUTUBE_CURRENCIES

def test_currencies() -> None:
    keys: KeysView = YOUTUBE_CURRENCIES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert YOUTUBE_CURRENCIES[key] is not None

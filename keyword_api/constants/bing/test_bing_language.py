from typing import KeysView
from keyword_api.constants import BING_LANGUAGES

def test_languages() -> None:
    keys: KeysView = BING_LANGUAGES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert BING_LANGUAGES[key] is not None

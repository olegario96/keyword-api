# @see https://docs.keywordtool.io/reference#search-volume-bing-metrics-location
LOCATIONS: dict = {
    'AUSTRALIA': 9,
    'CANADA': 32,
    'FRANCE': 66,
    'GERMANY': 72,
    'UNITED_KINGDOM': 188,
    'UNITED_STATES': 190,
}

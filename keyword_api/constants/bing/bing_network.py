# @see https://docs.keywordtool.io/reference#search-volume-bing-metrics-network
NETWORKS: dict = {
    'OWNED_AND_OPERATOR_AND_SYNDICATED': 'ownedandoperatedandsyndicatedsearch',
    'OWNED_AND_OPERATOR_ONLY': 'ownedandoperatedonly',
    'SYNDICATED_ONLY': 'syndicatedsearchonly',
}

from typing import KeysView
from keyword_api.constants import BING_NETWORKS

def test_networks() -> None:
    keys: KeysView = BING_NETWORKS.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert BING_NETWORKS[key] is not None

from typing import KeysView
from keyword_api.constants import BING_LOCATIONS

def test_locations() -> None:
    keys: KeysView = BING_LOCATIONS.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert BING_LOCATIONS[key] is not None

# @see https://docs.keywordtool.io/reference#search-volume-bing-metrics-language
LANGUAGES: dict = {
    'ENGLISH': 'English',
    'FRENCH': 'French',
    'GERMAN': 'German',
}

from typing import KeysView
from keyword_api.constants import EBAY_CURRENCIES

def test_currencies() -> None:
    keys: KeysView = EBAY_CURRENCIES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert EBAY_CURRENCIES[key] is not None

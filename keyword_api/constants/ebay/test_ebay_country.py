from typing import KeysView
from keyword_api.constants import EBAY_COUNTRIES

def test_countries() -> None:
    keys: KeysView = EBAY_COUNTRIES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert EBAY_COUNTRIES[key] is not None

from typing import List

# Amazon imports
from .amazon.amazon_country import COUNTRIES as AMAZON_COUNTRIES
from .amazon.amazon_currency import CURRENCIES as AMAZON_CURRENCIES

# Bing imports
from .bing.bing_language import LANGUAGES as BING_LANGUAGES
from .bing.bing_location import LOCATIONS as BING_LOCATIONS
from .bing.bing_network import NETWORKS as BING_NETWORKS

# Ebay imports
from .ebay.ebay_country import COUNTRIES as EBAY_COUNTRIES
from .ebay.ebay_currency import CURRENCIES as EBAY_CURRENCIES

# Google imports
from .google.google_currency import CURRENCIES as GOOGLE_CURRENCIES
from .google.google_language import LANGUAGES as GOOGLE_LANGUAGES
from .google.google_location import LOCATIONS as GOOGLE_LOCATIONS
from .google.google_network import NETWORKS as GOOGLE_NETWORKS

# Youtube imports
from .youtube.youtube_country import COUNTRIES as YOUTUBE_COUNTRIES
from .youtube.youtube_currency import CURRENCIES as YOUTUBE_CURRENCIES

# HTTP status code
from .http_status_code import *

########## GOOGLE SECTION ##########
SEARCH_VOLUME_GOOGLE_DEV_URL: str = 'https://api.keywordtool.io/v2-sandbox/search/volume/google'
SEARCH_VOLUME_GOOGLE_PROD_URL: str = 'https://api.keywordtool.io/v2/search/volume/google'

SEARCH_VOLUME_GOOGLE_METRICS: List = [
    {'metrics_currency': GOOGLE_CURRENCIES.values()},
    {'metrics_language': GOOGLE_LANGUAGES.values()},
    {'metrics_location': GOOGLE_LOCATIONS.values()},
    {'metrics_network': GOOGLE_NETWORKS.values()},
]

########## YOUTUBE SECTION ##########
SEARCH_VOLUME_YOUTUBE_DEV_URL: str = 'https://api.keywordtool.io/v2-sandbox/search/volume/youtube'
SEARCH_VOLUME_YOUTUBE_PROD_URL: str = 'https://api.keywordtool.io/v2/search/volume/youtube'

SEARCH_VOLUME_YOUTUBE_METRICS: List = [
    {'country': YOUTUBE_COUNTRIES.values()},
    {'metrics_currency': YOUTUBE_CURRENCIES.values()},
]

########## BING SECTION ##########
SEARCH_VOLUME_BING_DEV_URL: str = 'https://api.keywordtool.io/v2-sandbox/search/volume/bing'
SEARCH_VOLUME_BING_PROD_URL: str = 'https://api.keywordtol.io/v2/search/volume/bing'

SEARCH_VOLUME_BING_METRICS: List = [
    {'metrics_language': BING_LANGUAGES.values()},
    {'metrics_location': BING_LOCATIONS.values()},
    {'metrics_network': BING_NETWORKS.values()},
]

########## AMAZON SECTION ##########
SEARCH_VOLUME_AMAZON_DEV_URL: str = 'https://api.keywordtool.io/v2-sandbox/search/volume/amazon'
SEARCH_VOLUME_AMAZON_PROD_URL: str = 'https://api.keywordtool.io/v2/search/volume/amazon'

SEARCH_VOLUME_AMAZON_METRICS: List = [
    {'country': YOUTUBE_COUNTRIES.values()},
    {'metrics_currency': YOUTUBE_CURRENCIES.values()},
]

########## EBAY SECTION ##########
SEARCH_VOLUME_EBAY_DEV_URL: str = 'https://api.keywordtool.io/v2-sandbox/search/volume/ebay'
SEARCH_VOLUME_EBAY_PROD_URL: str = 'https://api.keywordtool.io/v2/search/volume/ebay'

SEARCH_VOLUME_EBAY_METRICS: List = [
    {'country': EBAY_COUNTRIES.values()},
    {'metrics_currency': EBAY_CURRENCIES.values()},
]

DEFAULT_OUTPUT: str = 'json'

from typing import KeysView
from keyword_api.constants import GOOGLE_NETWORKS

def test_networks() -> None:
    keys: KeysView = GOOGLE_NETWORKS.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert GOOGLE_NETWORKS[key] is not None

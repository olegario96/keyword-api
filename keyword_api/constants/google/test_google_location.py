from typing import KeysView
from keyword_api.constants import GOOGLE_LOCATIONS

def test_locations() -> None:
    keys: KeysView = GOOGLE_LOCATIONS.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert GOOGLE_LOCATIONS[key] is not None

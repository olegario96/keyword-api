from typing import KeysView
from keyword_api.constants import GOOGLE_CURRENCIES

def test_currencies() -> None:
    keys: KeysView = GOOGLE_CURRENCIES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert GOOGLE_CURRENCIES[key] is not None

# @see https://docs.keywordtool.io/reference#search-volume-google-metrics-network
NETWORKS: dict = {
    'GOOGLE': 'googlesearch',
    'GOOGLE_SEARCH_NETWORK': 'googlesearchnetwork',
}

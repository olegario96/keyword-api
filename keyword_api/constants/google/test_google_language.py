from typing import KeysView
from keyword_api.constants import GOOGLE_LANGUAGES

def test_languages() -> None:
    keys: KeysView = GOOGLE_LANGUAGES.keys()
    for key in keys:
        assert ' ' not in key
        assert key.isupper()
        assert GOOGLE_LANGUAGES[key] is not None

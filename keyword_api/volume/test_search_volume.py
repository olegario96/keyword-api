import pytest

# Imports are being used in args
# pylint: disable=unused-import
from keyword_api.utils.test_environment import fixture_dev_environment, fixture_prod_environment

from keyword_api.constants import DEFAULT_OUTPUT,\
                                  GOOGLE_CURRENCIES,\
                                  GOOGLE_LANGUAGES,\
                                  GOOGLE_LOCATIONS,\
                                  GOOGLE_NETWORKS,\
                                  SEARCH_VOLUME_GOOGLE_DEV_URL,\
                                  SEARCH_VOLUME_GOOGLE_PROD_URL,\
                                  SEARCH_VOLUME_YOUTUBE_DEV_URL,\
                                  SEARCH_VOLUME_YOUTUBE_PROD_URL,\
                                  SEARCH_VOLUME_BING_DEV_URL,\
                                  SEARCH_VOLUME_BING_PROD_URL,\
                                  SEARCH_VOLUME_AMAZON_DEV_URL,\
                                  SEARCH_VOLUME_AMAZON_PROD_URL,\
                                  SEARCH_VOLUME_EBAY_DEV_URL,\
                                  SEARCH_VOLUME_EBAY_PROD_URL

from keyword_api.volume import SearchVolume, VolumeType

@pytest.fixture(name='search_volume')
def fixture_search_volume():
    return SearchVolume()

@pytest.fixture(name='args')
def fixture_args():
    return {
        'apikey': 'foo',
        'keyword': ['python'],
        'metrics_location': [GOOGLE_LOCATIONS.get('BRAZIL')],
        'metrics_language': [GOOGLE_LANGUAGES.get('PORTUGUESE')],
        'metrics_network': [GOOGLE_NETWORKS.get('GOOGLE')],
        'metrics_currency': [GOOGLE_CURRENCIES.get('BRAZILIAN_REAL')]
    }

@pytest.fixture(name='sv_with_mocked_client')
def fixture_mocked_client(search_volume):
    # pylint: disable=too-few-public-methods
    class KeywordApiClient:
        '''
        MOCK CLASS
        '''
        # pylint: disable=no-self-use
        def post(self, _url, _args):
            return {}

    search_volume.cli = KeywordApiClient()
    return search_volume

def test_get_api_endpoint_on_prod(_prod_environment):
    assert SearchVolume.get_api_endpoint(VolumeType.GOOGLE) == SEARCH_VOLUME_GOOGLE_PROD_URL
    assert SearchVolume.get_api_endpoint(VolumeType.YOUTUBE) == SEARCH_VOLUME_YOUTUBE_PROD_URL
    assert SearchVolume.get_api_endpoint(VolumeType.BING) == SEARCH_VOLUME_BING_PROD_URL
    assert SearchVolume.get_api_endpoint(VolumeType.AMAZON) == SEARCH_VOLUME_AMAZON_PROD_URL
    assert SearchVolume.get_api_endpoint(VolumeType.EBAY) == SEARCH_VOLUME_EBAY_PROD_URL

def test_get_api_endpoint_on_dev(_dev_environment):
    assert SearchVolume.get_api_endpoint(VolumeType.GOOGLE) == SEARCH_VOLUME_GOOGLE_DEV_URL
    assert SearchVolume.get_api_endpoint(VolumeType.YOUTUBE) == SEARCH_VOLUME_YOUTUBE_DEV_URL
    assert SearchVolume.get_api_endpoint(VolumeType.BING) == SEARCH_VOLUME_BING_DEV_URL
    assert SearchVolume.get_api_endpoint(VolumeType.AMAZON) == SEARCH_VOLUME_AMAZON_DEV_URL
    assert SearchVolume.get_api_endpoint(VolumeType.EBAY) == SEARCH_VOLUME_EBAY_DEV_URL

def test_get_api_endpoint_for_invalid_type(_dev_environment):
    invalid_volume_type = 'invalid'
    with pytest.raises(Exception):
        SearchVolume.get_api_endpoint(invalid_volume_type)

def test_default_output_for_args(args):
    SearchVolume.ensure_valid_args(args, VolumeType.GOOGLE)
    assert args.get('output') == DEFAULT_OUTPUT

def test_search_volume(sv_with_mocked_client, args):
    assert sv_with_mocked_client.search_volume(args, VolumeType.GOOGLE) == {}

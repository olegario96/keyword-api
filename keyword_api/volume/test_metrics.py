import pytest

from keyword_api.constants import GOOGLE_CURRENCIES
from keyword_api.volume.metrics import validate_generic_metrics

@pytest.fixture(name='empty_values')
def fixture_empty_values():
    return []

@pytest.fixture(name='invalid_metric')
def fixture_invalid_metric():
    return ['foo']

@pytest.fixture(name='valid_metric')
def fixture_valid_metric():
    return [GOOGLE_CURRENCIES.get('BRAZILIAN_REAL')]

def test_validate_generic_metrics_empty_values(empty_values):
    with pytest.raises(Exception):
        validate_generic_metrics(empty_values, GOOGLE_CURRENCIES.values())

def test_validate_generic_metrics_invalid_metric(invalid_metric):
    with pytest.raises(Exception):
        validate_generic_metrics(invalid_metric, GOOGLE_CURRENCIES.values())

def test_validate_generic_metrics_valid_metric(valid_metric):
    assert validate_generic_metrics(valid_metric, GOOGLE_CURRENCIES.values()) is None

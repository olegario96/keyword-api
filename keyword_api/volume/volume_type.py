from typing import List

from enum import Enum

from keyword_api.constants import SEARCH_VOLUME_GOOGLE_METRICS,\
                                  SEARCH_VOLUME_YOUTUBE_METRICS,\
                                  SEARCH_VOLUME_BING_METRICS,\
                                  SEARCH_VOLUME_AMAZON_METRICS,\
                                  SEARCH_VOLUME_EBAY_METRICS

class VolumeType(Enum):
    '''
    Enum class for searches related to volume
    '''
    GOOGLE: str = 'google'
    YOUTUBE: str = 'youtube'
    BING: str = 'bing'
    AMAZON: str = 'amazon'
    EBAY: str = 'ebay'

def volume_type_to_metrics(volume_type: str) -> List:
    # Disabling linter here since the goal is to emulate a switch case statement
    # pylint: disable=no-else-return
    if volume_type == VolumeType.GOOGLE:
        return SEARCH_VOLUME_GOOGLE_METRICS
    elif volume_type == VolumeType.YOUTUBE:
        return SEARCH_VOLUME_YOUTUBE_METRICS
    elif volume_type == VolumeType.BING:
        return SEARCH_VOLUME_BING_METRICS
    elif volume_type == VolumeType.AMAZON:
        return SEARCH_VOLUME_AMAZON_METRICS
    elif volume_type == VolumeType.EBAY:
        return SEARCH_VOLUME_EBAY_METRICS

    raise Exception('volume_type {} provided is not valid!'.format(volume_type))

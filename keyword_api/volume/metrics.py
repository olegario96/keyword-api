from typing import List


def validate_generic_metrics(generic_metrics: List, metrics_values: List) -> None:
    if generic_metrics is None or not generic_metrics:
        raise Exception('Emptry generic_metrics provided!')

    for metric in generic_metrics:
        if metric not in metrics_values:
            raise Exception('Metric {} not available!'.format(metric))

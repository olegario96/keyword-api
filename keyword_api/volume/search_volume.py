from typing import List

from keyword_api.constants import DEFAULT_OUTPUT,\
                                  SEARCH_VOLUME_GOOGLE_DEV_URL,\
                                  SEARCH_VOLUME_GOOGLE_PROD_URL,\
                                  SEARCH_VOLUME_YOUTUBE_DEV_URL,\
                                  SEARCH_VOLUME_YOUTUBE_PROD_URL,\
                                  SEARCH_VOLUME_BING_DEV_URL,\
                                  SEARCH_VOLUME_BING_PROD_URL,\
                                  SEARCH_VOLUME_AMAZON_DEV_URL,\
                                  SEARCH_VOLUME_AMAZON_PROD_URL,\
                                  SEARCH_VOLUME_EBAY_DEV_URL,\
                                  SEARCH_VOLUME_EBAY_PROD_URL

from keyword_api.client.keyword_api_client import KeywordApiClient
from keyword_api.volume.metrics import validate_generic_metrics
from keyword_api.volume.volume_type import VolumeType, volume_type_to_metrics
from keyword_api.utils.args_validation import validate_common_args
from keyword_api.utils.environment import is_production_environment


class SearchVolume:
    '''
    Class that allows search by volume
    '''
    def __init__(self):
        self.cli = KeywordApiClient()

    def search_volume(self, args: dict, volume_type: str) -> dict:
        SearchVolume.ensure_valid_args(args, volume_type)
        url: str = SearchVolume.get_api_endpoint(volume_type)
        return self.cli.post(url, args)

    @staticmethod
    def ensure_valid_args(args: dict, volume_type: str) -> None:
        validate_common_args(args)
        search_metrics: List = volume_type_to_metrics(volume_type)
        for metric_object in search_metrics:
            for key in metric_object:
                validate_generic_metrics(args.get(key), metric_object.get(key))

        output: str = 'output'
        output_from_args: str = args.get(output)
        if output_from_args is None or output_from_args != DEFAULT_OUTPUT:
            args[output] = DEFAULT_OUTPUT

    @staticmethod
    def get_api_endpoint(volume_type: str) -> str:
        is_prod_environment: bool = is_production_environment()
        # Disabling linter here since the goal is to emulate a switch case statement
        # pylint: disable=no-else-return
        if volume_type == VolumeType.GOOGLE:
            return SEARCH_VOLUME_GOOGLE_PROD_URL if is_prod_environment else SEARCH_VOLUME_GOOGLE_DEV_URL
        elif volume_type == VolumeType.YOUTUBE:
            return SEARCH_VOLUME_YOUTUBE_PROD_URL if is_prod_environment else SEARCH_VOLUME_YOUTUBE_DEV_URL
        elif volume_type == VolumeType.AMAZON:
            return SEARCH_VOLUME_AMAZON_PROD_URL if is_prod_environment else SEARCH_VOLUME_AMAZON_DEV_URL
        elif volume_type == VolumeType.BING:
            return SEARCH_VOLUME_BING_PROD_URL if is_prod_environment else SEARCH_VOLUME_BING_DEV_URL
        elif volume_type == VolumeType.EBAY:
            return SEARCH_VOLUME_EBAY_PROD_URL if is_prod_environment else SEARCH_VOLUME_EBAY_DEV_URL

        raise Exception('volume_type {} provided is not valid!'.format(volume_type))

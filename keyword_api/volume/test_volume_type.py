import pytest

from keyword_api.constants import SEARCH_VOLUME_GOOGLE_METRICS,\
                                  SEARCH_VOLUME_YOUTUBE_METRICS,\
                                  SEARCH_VOLUME_BING_METRICS,\
                                  SEARCH_VOLUME_AMAZON_METRICS,\
                                  SEARCH_VOLUME_EBAY_METRICS

from .volume_type import VolumeType
from .volume_type import volume_type_to_metrics

@pytest.fixture(name='volume_metrics_by_volume_type')
def fixture_volume_types():
    return {
        VolumeType.GOOGLE: SEARCH_VOLUME_GOOGLE_METRICS,
        VolumeType.YOUTUBE: SEARCH_VOLUME_YOUTUBE_METRICS,
        VolumeType.BING: SEARCH_VOLUME_BING_METRICS,
        VolumeType.AMAZON: SEARCH_VOLUME_AMAZON_METRICS,
        VolumeType.EBAY: SEARCH_VOLUME_EBAY_METRICS,
    }

@pytest.fixture(name='invalid_volume_types')
def fixture_invalid_volume_types():
    return [
        'DarthPlagueis'
    ]

def test_volume_type_to_metrics_with_valid_types(volume_metrics_by_volume_type):
    for volume_type, volume_metrics in volume_metrics_by_volume_type.items():
        assert volume_type_to_metrics(volume_type) == volume_metrics

def test_volume_type_to_metrics_with_invalid_types(invalid_volume_types):
    for invalid_volume_type in invalid_volume_types:
        with pytest.raises(Exception):
            volume_type_to_metrics(invalid_volume_type)
